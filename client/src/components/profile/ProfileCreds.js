import React, { Component } from "react";
import Moment from "react-moment";

class ProfileCreds extends Component {
  render() {
    const { experience, education } = this.props;
    const expItems = experience.map((exp) => (
      <li key={exp._id} class="list-group-item">
        <h4>{exp.company}</h4>
        <p>
          <strong>From</strong> <Moment format="YYYY/DD/MM">{exp.from}</Moment>{" "}
          - 
          {exp.to === null ? (
            "Now"
          ) : (
            <Moment format="YYYY/DD/MM">{exp.to}</Moment>
          )}
        </p>
        <p>
          <strong>Position:</strong> {exp.title}
        </p>
        <p>
          {exp.location === "" ? null : (
            <span>
              <strong>Location:</strong> {exp.location}
            </span>
          )}
        </p>
        <p>
          {exp.description === "" ? null : (
            <span>
              <strong>Decsription:</strong> {exp.description}
            </span>
          )}
        </p>
      </li>
    ));
    const eduItems = education.map((edu, index) => (
      <li class="list-group-item">
        <h4>{edu.school}</h4>
        <p><strong>From</strong>
          <Moment format="YYYY/DD/MM">{edu.from}</Moment>
        
        -
        {edu.to === null ? (
          "Now"
        ) : (
          <Moment format="YYYY/DD/MM">{edu.to}</Moment>
        )}
        </p>
        <p>
          <strong>Degree: </strong>
          {edu.degree}
        </p>
        <p>
          <strong>Field Of Study: </strong>
          {edu.fieldofstudy}
        </p>
        <p>
          {edu.description === '' ? null : (
            <span>
              <strong>Description:</strong> {edu.description}
            </span>
          )}
        </p>
      </li>
    ));
    return (
      <div class="row">
        <div class="col-md-6">
          <h3 class="text-center text-info">Experience</h3>
          {expItems.length > 0 ? (
            <ul class="list-group">{expItems}</ul>
          ) : (
            <ul class="list-group">No Experience Listed</ul>
          )}
        </div>
        <div class="col-md-6">
          <h3 class="text-center text-info">Education</h3>
          {eduItems.length > 0 ? (
            <ul class="list-group">{eduItems}</ul>
          ) : (
            <ul class="list-group">
              <li class="list-group-item">
                <h4>No Education Listed</h4>
              </li>
            </ul>
          )}
        </div>
      </div>
    );
  }
}
export default ProfileCreds;
